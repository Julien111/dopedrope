let btn = document.getElementById("bars");
let burger = document.getElementById("burger");

btn.addEventListener("click", function () {
  if (burger.style.display === "none") {
    burger.style.display = "flex";
  } else {
    burger.style.display = "none";
  }
});
